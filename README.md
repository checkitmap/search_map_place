# Google Maps Search Bar for Flutter
This is a fork of github.com/Bernardi23's flutter package 
[search_map_place](https://github.com/Bernardi23/search_map_place), also 
available on [pub.dev](https://pub.dev/packages/search_map_place).

For documentation see the README on the github repository linked above.

Modifications that have been made:
- Fixed layout issues as mentioned 
[here](https://github.com/Bernardi23/search_map_place/issues/8).
- Changed pubspec.yaml to use our own version of 
[google_maps_flutter](https://gitlab.com/checkitmap/google_maps_flutter)
to fix errors when running ```flutter packages get``` due to version conflicts.
- Removed example as this can be found on the original repository.
- Renamed searchMapPlaceWidget.dart to search_map_place_widget.dart to match 
flutter naming conventions.
- Added an optional leading IconButton
- Added an optional trailing IconButton in the place of the fixed icon
- Fixed issues with query results not disappearing when there's no text in the
search bar.


To use, declare in pubspec.yaml:
```
dependencies:
  search_map_place:
    git:
      url: https://gitlab.com/checkitmap/search_map_place.git
```

Import into a .dart file:
```
import 'package:search_map_place/search_map_place.dart';
```

Then follow example usage on original repository.
